CREATE TABLE users
(
    id         INT       NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    username   VARCHAR   NOT NULL,
    password   VARCHAR   NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NULL
);

CREATE TABLE phones
(
    id         INT       NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    make       VARCHAR   NOT NULL,
    model      VARCHAR   NOT NULL,
    booked_at  TIMESTAMP NULL,
    booked_by  INT       NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NULL,

    CONSTRAINT phones_users_fk FOREIGN KEY (booked_by) REFERENCES users (id)
);

INSERT INTO users(username, password)
VALUES ('kek1', '$2a$12$Qz5ejv1KLJKZ5twOJ5H7KugocVPW6wnaBs/BlQLteLTKo9hr4kiHO'),
       ('kek2', '$2a$12$Qz5ejv1KLJKZ5twOJ5H7KugocVPW6wnaBs/BlQLteLTKo9hr4kiHO'),
       ('kek3', '$2a$12$Qz5ejv1KLJKZ5twOJ5H7KugocVPW6wnaBs/BlQLteLTKo9hr4kiHO'),
       ('kek4', '$2a$12$Qz5ejv1KLJKZ5twOJ5H7KugocVPW6wnaBs/BlQLteLTKo9hr4kiHO');

