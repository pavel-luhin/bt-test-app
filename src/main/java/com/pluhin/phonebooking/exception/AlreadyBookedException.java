package com.pluhin.phonebooking.exception;

public class AlreadyBookedException extends RuntimeException {
    public AlreadyBookedException(Integer phoneId, String bookedUsername) {
        super(String.format("Phone with id %s already booked by user %s", phoneId, bookedUsername));
    }
}
