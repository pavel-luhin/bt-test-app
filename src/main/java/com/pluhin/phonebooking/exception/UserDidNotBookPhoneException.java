package com.pluhin.phonebooking.exception;

public class UserDidNotBookPhoneException extends RuntimeException {
    public UserDidNotBookPhoneException(Integer phoneId, String username) {
        super(String.format("User %s did not book the phone %s", username, phoneId));
    }
}
