package com.pluhin.phonebooking.exception;

public class NotBookedException extends RuntimeException {
    public NotBookedException(Integer id) {
        super(String.format("Phone %s is not booked", id));
    }
}
