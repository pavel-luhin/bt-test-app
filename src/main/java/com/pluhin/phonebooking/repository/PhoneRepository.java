package com.pluhin.phonebooking.repository;

import com.pluhin.phonebooking.entity.PhoneEntity;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PhoneRepository extends CrudRepository<PhoneEntity, Integer> {

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("FROM phones WHERE id = :id")
    Optional<PhoneEntity> findByIdWithLock(Integer id);

    Optional<PhoneEntity> findByMakeAndModel(String make, String model);
}
