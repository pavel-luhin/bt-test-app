package com.pluhin.phonebooking.entity;

import jakarta.persistence.*;

import java.time.LocalDateTime;

@Entity(name = "phones")
public class PhoneEntity extends BaseEntity {

    @Column
    private String make;

    private String model;

    @Column
    private LocalDateTime bookedAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "booked_by")
    private UserEntity bookedUser;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public LocalDateTime getBookedAt() {
        return bookedAt;
    }

    public void setBookedAt(LocalDateTime bookedAt) {
        this.bookedAt = bookedAt;
    }

    public void setBookedUser(UserEntity bookedUser) {
        this.bookedUser = bookedUser;
    }

    public UserEntity getBookedUser() {
        return bookedUser;
    }
}
