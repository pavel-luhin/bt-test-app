package com.pluhin.phonebooking.service;

import com.pluhin.phonebooking.entity.PhoneEntity;
import com.pluhin.phonebooking.entity.UserEntity;
import com.pluhin.phonebooking.exception.AlreadyBookedException;
import com.pluhin.phonebooking.exception.NotBookedException;
import com.pluhin.phonebooking.exception.NotFoundException;
import com.pluhin.phonebooking.exception.UserDidNotBookPhoneException;
import com.pluhin.phonebooking.repository.PhoneRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Objects;

@Service
public class PhoneBookingService {

    private final PhoneRepository phoneRepository;

    public PhoneBookingService(PhoneRepository phoneRepository) {
        this.phoneRepository = phoneRepository;
    }

    @Transactional
    public PhoneEntity getBookInfo(Integer id) {
        PhoneEntity entity = phoneRepository.findById(id).orElseThrow(() ->
                new NotFoundException(String.format("Phone with id %s was not found", id))
        );

        return entity;
    }

    public Iterable<PhoneEntity> getAllPhones() {
        return phoneRepository.findAll();
    }

    @Transactional
    public void returnPhone(Integer id) {
        PhoneEntity entity = phoneRepository.findByIdWithLock(id).orElseThrow(() ->
                new NotFoundException(String.format("Phone with id %s was not found", id))
        );

        String currentUsername = ((UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();

        if (entity.getBookedUser() == null) {
            throw new NotBookedException(id);
        }

        if (!Objects.equals(entity.getBookedUser().getUsername(), currentUsername)) {
            throw new UserDidNotBookPhoneException(id, entity.getBookedUser().getUsername());
        }

        entity.setBookedAt(null);
        entity.setBookedUser(null);
        phoneRepository.save(entity);
    }

    @Transactional
    public void bookPhone(Integer id) {
        PhoneEntity entity = phoneRepository.findByIdWithLock(id).orElseThrow(() ->
                new NotFoundException(String.format("Phone with id %s was not found", id))
        );

        if (entity.getBookedAt() != null) {
            throw new AlreadyBookedException(id, entity.getBookedUser().getUsername());
        }

        entity.setBookedUser((UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        entity.setBookedAt(LocalDateTime.now());
        phoneRepository.save(entity);
    }
}
