package com.pluhin.phonebooking.dto.phone;

public class PhoneDTO {

    private final Integer id;
    private final String make;
    private final String model;

    public PhoneDTO(Integer id, String make, String model) {
        this.id = id;
        this.make = make;
        this.model = model;
    }

    public Integer getId() {
        return id;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }
}
