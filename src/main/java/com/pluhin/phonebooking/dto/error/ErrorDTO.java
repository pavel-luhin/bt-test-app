package com.pluhin.phonebooking.dto.error;

public class ErrorDTO {

    private final String message;

    public ErrorDTO(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
