package com.pluhin.phonebooking.dto.booking;

import java.time.LocalDateTime;

public class BookInfoResponseDTO {

    private final String bookedBy;
    private final LocalDateTime bookedAt;

    public BookInfoResponseDTO(String bookedBy, LocalDateTime bookedAt) {
        this.bookedBy = bookedBy;
        this.bookedAt = bookedAt;
    }

    public String getBookedBy() {
        return bookedBy;
    }

    public LocalDateTime getBookedAt() {
        return bookedAt;
    }
}
