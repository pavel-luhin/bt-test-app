package com.pluhin.phonebooking.controller;

import com.pluhin.phonebooking.dto.error.ErrorDTO;
import com.pluhin.phonebooking.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class BookingExceptionHandler {

    @ExceptionHandler({
            AlreadyBookedException.class,
            NotBookedException.class,
            UserDidNotBookPhoneException.class,
    })
    public ResponseEntity<ErrorDTO> handleBadRequestException(RuntimeException ex) {
        return ResponseEntity.badRequest().body(
                new ErrorDTO(ex.getMessage())
        );
    }

    @ExceptionHandler({
            NotFoundException.class
    })
    public ResponseEntity<ErrorDTO> handleNotFoundException(RuntimeException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ErrorDTO(ex.getMessage())
        );
    }

    @ExceptionHandler({
            AccessDeniedException.class
    })
    public ResponseEntity<ErrorDTO> handleForbiddenException(RuntimeException ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                new ErrorDTO(ex.getMessage())
        );
    }
}
