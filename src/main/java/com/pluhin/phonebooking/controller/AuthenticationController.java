package com.pluhin.phonebooking.controller;

import com.pluhin.phonebooking.dto.authentication.AuthenticationRequestDTO;
import com.pluhin.phonebooking.dto.authentication.AuthenticationResponseDTO;
import com.pluhin.phonebooking.util.JwtHelper;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/authenticate")
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;

    public AuthenticationController(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @PostMapping()
    public ResponseEntity<AuthenticationResponseDTO> authenticate(
            @RequestBody AuthenticationRequestDTO authenticationDTO
    ) {
        UsernamePasswordAuthenticationToken upToken =
                new UsernamePasswordAuthenticationToken(
                        authenticationDTO.getUsername(),
                        authenticationDTO.getPassword()
                );
        authenticationManager.authenticate(upToken);
        String token = JwtHelper.generateToken(authenticationDTO.getUsername());
        return ResponseEntity.ok(new AuthenticationResponseDTO(token));
    }
}
