package com.pluhin.phonebooking.controller;

import com.pluhin.phonebooking.dto.booking.BookInfoResponseDTO;
import com.pluhin.phonebooking.entity.PhoneEntity;
import com.pluhin.phonebooking.service.PhoneBookingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/bookings")
public class PhoneBookingController {

    private final PhoneBookingService service;

    public PhoneBookingController(PhoneBookingService service) {
        this.service = service;
    }

    @GetMapping("/phone/{phoneId}")
    public ResponseEntity<BookInfoResponseDTO> getBookingInfo(
            @PathVariable("phoneId") Integer phoneId
    ) {
        PhoneEntity phone = service.getBookInfo(phoneId);
        if (phone.getBookedAt() == null) {
            return ResponseEntity.ok(new BookInfoResponseDTO(null, null));
        }

        return ResponseEntity.ok(
                new BookInfoResponseDTO(
                        phone.getBookedUser().getUsername(),
                        phone.getBookedAt()
                )
        );
    }

    @PostMapping("/phone/{phoneId}")
    public ResponseEntity<Void> bookPhone(@PathVariable("phoneId") Integer phoneId) {
        service.bookPhone(phoneId);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/phone/{phoneId}")
    public ResponseEntity<Void> returnPhone(@PathVariable("phoneId") Integer phoneId) {
        service.returnPhone(phoneId);
        return ResponseEntity.noContent().build();
    }
}
