package com.pluhin.phonebooking.controller;

import com.pluhin.phonebooking.dto.phone.PhoneDTO;
import com.pluhin.phonebooking.service.PhoneBookingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/api/phones")
public class PhoneController {

    private final PhoneBookingService service;

    public PhoneController(PhoneBookingService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<PhoneDTO>> getAllPhones() {
        return ResponseEntity.ok(
                StreamSupport.stream(service.getAllPhones().spliterator(), false)
                        .map((phoneEntity -> new PhoneDTO(
                                phoneEntity.getId(),
                                phoneEntity.getMake(),
                                phoneEntity.getModel()
                        )))
                        .toList()
        );
    }
}
